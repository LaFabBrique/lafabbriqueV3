# La Fab'Brique
*Fabrique (presque) tout, avec (presque) rien*

----

## À propos
La Fab'Brique est une initiative associative de **Laboratoire de Fabrication** ( [FabLab](https://fr.wikipedia.org/wiki/Fab\_lab) ) basé dans le Béarn des Gaves

**C'est un lieu unique et convivial de création et d'innovation**

**La Fab'Brique** propose un espace dédié:
   * À la **rencontre** entre les individus
   * À l’**échange** et au **partage** d’**idées** et de **compétences techniques**
   * À la **conception et au développement d’objets, machines ou autre**
   * À la **fabrication de prototypes**

**L'association** a pour objet :
   * De favoriser **l’échange de savoir-faire et de connaissances**
   * De proposer **des ateliers et des formations**
   * De mettre à disposition **des moyens techniques**
   * De participer à l'enrichissement d'un **bien commun technique et technologique**
   * D'établir **des liens** avec d'autres structures, associatives ou non

**Charte d'utilisation globale:**
   * **Fabriquer par soi-même** (en apprenant avec d’autres)
   * **Ne rien fabriquer qui puisse nuire**
   * **Ne blesser personne et ne pas endommager le matériel**
   * **Aider à nettoyer, maintenir et améliorer le local**
   * **Contribuer à la documentation et partager ses connaissances**

----

## Informations

### Le lieu

Un FabLab, c'est avant tout un lieu, dans lequel des moyens de conception et de fabrication sont accessibles. On vient aussi y trouver et échanger des compétences et des connaissances.

Depuis juillet 2017, **La Fab'Brique** a intégré l'un des ateliers de **[La Station](https://www.espace-station.fr/)**, le pôle de développement économique de la Communauté de Communes du Béarn des Gaves (Sauveterre). Cet espace de 80m2 permet à la structure de s'organiser, de s'équiper, de voir naître des projets et d’accueillir du public. L'espace peut aussi être privatisé à la journée par les entreprises désireuses d'utiliser des outils de prototypage rapide et de R\&D.

Profitant de la localisation des membres de l'association, La Fab'Brique n'exclue pas d'ouvrir des lieux annexes, et d'animer des ateliers dans les cantons environnants de Salies/Sauveterre.

**Module3@La Station**  
D27 - 64390 Sauveterre-de-Béarn

*[iframe carte](http://www.openstreetmap.org/?mlat=43.47207&mlon=-0.92633#map=19/43.47207/-0.92633)*

### L'association
Un FabLab, c'est aussi une communauté. Un réseau de membres et de structures qui partagent leurs savoirs, et s'entraident dans la réalisation de leurs projets.

L'association est ouverte à tous. Particuliers, étudiants, entrepreneurs, artisans, TPE/PME, institutions, écoles et centre de loisirs... L'adhésion est de 50€ pour une année (septembre à août), et donne accès au local et à son équipement.

   * Bouton "**[Adhérez à l'association](https://www.helloasso.com/associations/la-fab-brique/adhesions/adhesion-adhesion-2018-19)**"
Forte des briques de compétences diverses qu'apportent les membres, il est possible de trouver au sein de l'association des personnes pouvant servir de référents sur des aspects techniques particuliers. Un **[tableau de références](http://lafabbrique.org/wiki/lafabbrique:tableau\_de\_references)** est disponible dans la documentation.

La Fab'Brique œuvrant dans un objectif d'éducation populaire, chaque membre peut proposer d'organiser et animer des ateliers et des formations ouverts à tous. La liste des événements est consultable dans l'agenda.

*[iframe agenda](https://framagenda.org/index.php/apps/calendar/public/7ZEMRPSRHI819PV8)*

**En savoir plus sur l'association:**
   * **[Consulter les statuts](http://lafabbrique.org/wiki/lafabbrique:statuts)**
   * **[Consulter le règlement intérieur](http://lafabbrique.org/wiki/lafabbrique:reglement\_interieur)**

### Le matériel
Au sein d'un FabLab, on trouve généralement des outils de conception et de fabrication numérique (CAO/DAO, CNC, imprimante 3D...). Le matériel est mutualisé, et mis à disposition des usagers afin qu'ils puissent réaliser leurs projets.

À **La Fab'Brique**, on trouve du matériel informatique, électronique, de découpe, d'usinage, de formage, accessible aux membres de l'association. Si certaines machines nécessitent une formation particulière, celle-ci est dispensée par des [membres référents](http://lafabbrique.org/wiki/lafabbrique:tableau\_de\_references). Par ailleurs, chaque machine possède une fiche d'instruction dans la documentation.

Chaque utilisateur participe à l'achat, à l'amélioration et à l'entretien du matériel par sa cotisation, et en payant ses consommables. Le matériel et les machines peuvent aussi être issus de la récupération ou du don.

   * Liste du matériel et des machines: **[Voir la catégorie matériel de la documentation](http://lafabbrique.org/wiki/materiel:materiel)**
   * Consommables et matériels divers: **Voir la grille tarifaire**

### Les projets
Du bricolage au prototypage, de la réparation à l'innovation, tous les projets trouvent leur place au sein de **La Fab'Brique**.

Les projets sont au coeur du FabLab. Ce sont eux qui provoquent les échanges, qui dynamisent le lieu et permettent l'émergence de créations collaboratives.

Lors des permanences, les adhérents peuvent utiliser l’espace, le matériel et les compétences disponibles à La Fab’Brique pour avancer sur leur projet. Lorsque le projet est terminé, il appartient à l’utilisateur qui l’a réalisé.

Afin de contribuer à l'élaboration d'un bien commun technique et technologique, les projets réalisés au sein de La Fab'Brique sont documentés, et cette documentation est accessible à tous, sans restriction (principe de l'OpenSource). Ceci permet que chaque projet puisse être transmis, réutilisé, dérivé et amélioré par tous.

Par ailleurs, les activités commerciales sont pleinement encouragées à se créer au sein de la Fab’Brique avec pour objectif de prendre leur autonomie.
Si un projet donne lieu à la fabrication d’un prototype qui peut conduire à la création d’une société, la Fab’Brique accompagne l’utilisateur vers les structures qui lui permettent de mûrir son projet entrepreneurial (les services de développement économique de la communauté de communes sont basés dans les même locaux).

   * Bouton "**[Voir la liste des projets](http://lafabbrique.org/wiki/projets:projets)**"

### La documentation
Les pages de la documentation permettent de recueillir les sources et ressources de chaque projet réalisé à La Fab'Brique. La licence qui l'encadre ([CC-BY 2.0](http://creativecommons.org/licenses/by/2.0/fr/)) permet à chacun de la partager et de l'adapter, sans restriction.

On trouve aussi dans la documentation: toutes les informations utiles à la vie de l'association, les notices d'utilisation du matériel, les sources et les supports des ateliers et des formations, etc...

La documentation est collaborative. Elle peut être rédigée, commentée, corrigée et enrichie par tous.

   * Bouton "**[Accéder à la documentation](http://lafabbrique.org/wiki)**"
Dépôt commun pour "le code"

   * Bouton "**[lafabbrique@framagit.org](https://framagit.org/lafabbrique)**"

### Les partenaires

Ils nous soutiennent:
   * Logos partenaires

Ils parlent de nous:
   * Voir [la page dédiée dans la documentation](http://lafabbrique.org/wiki/lafabbrique:medias)

Pour nous soutenir:
   * Bouton "**[Adhérez à l'association](https://www.helloasso.com/associations/la-fab-brique/adhesions/adhesion-adhesion-2018-19)**"
   * Bouton "**[Faire un don](https://www.helloasso.com/associations/la-fab-brique/formulaires/1)**"

Parler de nous autour de vous:
   * Bouton "**[Facebook](https://www.facebook.com/LaFabBrique/?ref=br_rs)**"
   * Bouton "**[Diaspora](https://framasphere.org/people/47619ff02133013457df2a0000053625)**"

## Testimonial
Le but de **La Fab’Brique** est de favoriser la rencontre entre les individus qui *veulent* faire et les individus qui *savent* faire
*Pas pour que ceux qui savent faire réalisent l’idée de ceux qui veulent faire...*
*mais pour apprendre à ceux qui veulent faire, à faire... C’est clair ?!*

## Contact
Pour toute question, n'hésitez pas à nous écrire:
contact[@]lafabbrique[.]org

## Footer
### Adresse                        
**[Module3@La Station](http://www.openstreetmap.org/?mlat=43.47207\&mlon=-0.92633#map=19/43.47207/-0.92633)**  
D27 - 64390 Sauveterre-de-Béarn

### Permanences
**jeu. 19h30, 1/2 sam. 14h00**  
[Voir l'agenda](https://framagenda.org/index.php/apps/calendar/public/7ZEMRPSRHI819PV8)

### Adhésions
**50€/an** *(sept-aout)*  
[Adhésion en ligne](https://www.helloasso.com/associations/la-fab-brique/adhesions/adhesion-adhesion-2018-19)

  * Boutons "médias sociaux" ([FB](https://www.facebook.com/LaFabBrique/?ref=br_rs), [D*](https://framasphere.org/people/47619ff02133013457df2a0000053625), [Gitlab](https://framagit.org/LaFabBrique))

## Mentions légales
### La Fab'Brique                            
La Fab'Brique est une association à but non lucratif régie par la loi du 6 janvier 1901, déclarée le 5 novembre 2015 sous le numéro W643007228 auprès de la Préfecture de Pau (64).
Les statuts et le règlement intérieur sont consultables dans la documentation.
                            
### Ce site
#### Propriété
Les pages de ce site sont rédigées et administrées par leur propriétaire:
    
**Association La Fab'Brique**  
31, avenue Al Cartero - 64270 Salies-de-Béarn - France  
contact [@] lafabbrique [.] org

#### Hébergement                            
Ces pages sont hébergées sur un serveur mutualisé appartenant à:

**[OVH](https://www.ovh.com/fr/support/documents\_legaux/)**  
2, rue Kellermann - 59100 Roubaix - France.  
N° TVA : FR 22 424 761 419  
RCS Lille Métropole 424 761 419 00045

#### Vie privée                            
Ce site ne réclame et ne collecte aucune information privée ou technique. Aucun système d'analyse ni de statistiques n'est mis en place sur ce site par son propriétaire. Néanmoins, l'hébergeur (OVH) de se site possédant et utilisant des outils d'analyse et de statistiques, il est possible que des données vous appartenant soient collectées par ce prestataire. Dans ce cas, La Fab'Brique ne saurait être tenue responsable de l'utilisation de ces données.

#### Enregistrement et connexion                            
Aucun enregistrement, ni aucune connexion, n'est nécessaire afin de consulter ce site et la documentation. Néanmoins, et afin de préserver la qualité de la documentation *(en évitant les robots spammeurs)*, la contribution à celle-ci nécessite un enregistrement *([en savoir plus...](http://lafabbrique.org/wiki/aide:faq#compte))*. Dans ce cadre, aucune information de connexion ou d'identité ne saurait être divulguée à des tiers.

#### Contenus et ressources                            
La structure *(html)* et le style *(css)* des pages de ce site sont dérivés du thème Freelancer par StartBootstrap, créé pour le framework Bootstrap *(Licence MIT)*.
Le code de ces pages est disponible sur [Framagit](https://framagit.org/LaFabBrique/lafabbrique\_freelancer), et placé sous les même termes de licence que le code original.

Les pages de ce site et les illustrations sont réalisées en utilisant des logiciels libres: [Geany](http://www.geany.org/) *(éditeur de texte)*, [Inkscape](http://inkscape.org/?lang=fr) *(publication, dessin vectoriel)*. Tous deux sous licence [GNU/GPL](http://www.gnu.org/licenses/gpl.html).

Les ressources graphiques utilisées pour l'élaboration de ces pages sont issues de la librairie [OpenClipArt](http://openclipart.org/) *([domaine public](http://creativecommons.org/publicdomain/zero/1.0/deed.fr))*. Par ailleurs, les créations originales et dérivées crées pour ce site ont été reversées à la librairie OpenClipArt.

Les pages de la documentation sont propulsées par [DokuWiki](https://www.dokuwiki.org/). Un moteur de wiki simple et léger, sans base de données, développé par *Andreas Gohr et la communauté DokuWiki*. Le logiciel est placé sous licence [GNU/GPL](http://www.gnu.org/licenses/gpl.html).

Sauf mention contraire, les contenus (textes et fichiers) qui composent ce site et la documentation sont placés sous licence [CC-BY 2.0 FR](http://creativecommons.org/licenses/by/2.0/fr/) (Licence Creative Commons Attribution 2.0 France).
