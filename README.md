_Pad de travail: https://annuel2.framapad.org/p/lafabbriqueV3_

----

# Site et outils web La Fab'Brique Version 3.0

## Satut
[W.I.P]...

## Cahier des charges
### Vitrine
Informations générales, communication externe/publique. Composée de pages statiques (déjà créées pour la V2, cf. détails et textes bruts: [./textes_bruts.md](./textes_bruts.md))

  - À propos
    - Infos
    - Le lieu    
Carte (https://www.openstreetmap.org/?mlat=43.4020&mlon=-0.9495#map=15/43.4020/-0.9495)
    - L'association    
Trombinoscope des membres (avec localisation géographique sur une carte: https://framacarte.org/fr/map/la-fabbrique_14829?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=undefined&captionBar=false)
    - Le matériel    
Liste dynamique du matériel et des machines disponibles
    - Les projets    
Liste dynamique des projets avec leur(s) référent(s) et statut
    - La documentation
    - Les partenaires
      - Logos/liens partenaires
      - Liens vers les chroniques et le Dossier de presse
      - Boutons "adhésion" et "dons"
      - Boutons médias sociaux
    - Mentions légales

### Com' externe
  - Formulaire de contact
  - Abonnement newsletter
  - Abonnement flux RSS
  - Calendrier
    - Affiché sur le site (consult/com publique)
    - Éditable par les membres (ajout évènements/déclaration conciergerie) avec validation par un modérateur ?

### Documentation
Utilisée pour documenter le matériel, en tant que support pour les membres, et afin satisfaire au principe d'OpenSource des éléments produits par un FabLab.
La documentation doit permettre la rédaction collaborative de ses pages, l'enrichissement/amélioration continue par des tiers (membres/enregistrés) et la diffusion/réutilisation légale des contenus qui la compose (licence type CC-BY: http://creativecommons.org/licenses/by/2.0/fr/ )

  - Contenus (par catégories):
    - Pages "Projets" communs/persos
    - Pages "Lieu" (charte d'utilisation/gestion du local)
    - Pages "Association"
      - Pages publiques (réglement intérieur, statuts, compte-rendus d'AG)
      - Pages privées (compta, administration, stockage de fichiers et d'infos "sensibles")
    - Pages "Machines/matériel" (fiche d'instruction/sécurité)
    - Pages "Tutoriels" (astuces, instructions particulières matérielles/logicielles...)
    - Pages "Ressources" (les sources de tous les éléments créées et dérivées par les contributeurs membres du FabLab)
      - Ressources graphiques
      - Ressources rédactionnelles (supports d'ateliers/formations)
      - Code informatique (l'utilisation d'un service/logiciel de gestion de version -type GIT- est plus appropriéé pour ces ressources particulières)
    - ...

### News et annonces
Partie "blog" avec possibilité de rédaction collaborative (pour relectures/corrections) et processus de validation des articles (par les gestionnaires/modérateurs) avant leur mise en ligne.

  - Comité rédactionnel restreint
  - Équipe de gestion/modération
  - Infos ponctuelles (en // à la newsletter, pour l'archivage et la com' externe globale)
  - Évènements et rapport d'évènements (vie de l'asso, animations/ateliers...)
  - Système de commentaires visiteurs (avec processus de validation -anti-spams)
  - Flux RSS
  - Syndication avec les médias sociaux (quand on poste un article -*d'un certain type="news"*- sur le blog, ça poste auto sur FB/D*...)

### Support et discutions

  - Forum (modéré)
    - Partie privée (bureau/CA/admin)
    - Partie publique / discutions générales
    - Sous-forum projets dédié aux discutions internes sur le développement des projets en cours     
"zone grise", publique? privée? Il ne faudrait pas se priver d'aide ou de conseils potentiels de membres non-inscrits sur des projets communs..

## Éthique et vie privée

Ensemble d'actions de protection et de respect autour de la vie privée des visiteurs/utilisateurs du site.
M'est avis qu'à notre époque (hégémonie des GAFAs, internet centralisé et marchand, big-data à usage commerciale et repressif...), et dans la perspective de la construction d'un web ouvert, neutre et respectueux, ces questions sont essentielles et doivent être débattues. Aussi, l'utilisation de certains outils doit être réfléchie (ou plutôt l'absence d'utilisation de certains outils doit être ordonnée).

  - *Problématique* => **alternative**
  - *Utilisation des "boutons facebook, G+, Twitter..." (ce sont des trackers d'activités, même pour les personnes non-inscrites à ces réseaux)* => **Utiliser la "double activation" si boutons, ou des liens directs**
  - *Utilisation des services tiers d'analyse et de statistiques de fréquentation* => **service autogéré (Piwik), ou rien... (quel intérêt de traquer les visites?)**
  - *Utilisation de webfonts (GoogleFonts, Glyphio...) qui transmettent des données au fournisseur* => **Utiliser des freefonts et les embarquer**
  - *Utilisation de cookies tiers* => **Limiter les cookies à ceux utiles au fonctionnement strict du site, avec possibilité de destruction auto en fin de session**
  - *Utilisation de services tiers nécessitants une autentification supplémentaire (Gravatar, Disqus, "Se connecter avec xxx"...)* => **services autogérés (Plugins autonomes pour les commentaires de blog...)**
  - 


## Timeline
### Alpha
  - [x] Install dans une machine virtuelle (Virtualbox VDI) sur mon poste perso
  - [ ] Sur une machine serveur test à La Fab'Brique
  - [x] Synchro du template et de la BDD sur un dépôt GIT: https://framagit.org/LaFabBrique/lafabbriqueV3

Deadline: **xx/xx/2018**

### Beta privée
  - [ ] Sur hébergement OVH, dans un sous-dossier "V3"

Deadline: **xx/xx/2018**    


### Mise en PROD

Deadline: **xx/xx/2018**

## Tâches connexes

  - [ ] Définir les addons/plugin (https://extensions.joomla.org)
    - [ ] Forum (parties publiques/privés)
      - ...
    - [ ] Documentation (publique et collaborative)
      - ...
    - [ ] News letter
      - ...
    - [ ] Formulaire de contact
      - ...
    - [ ] Syndication réseaux sociaux (?)
      - ...
  - [ ] Définir les groupes et autorisations
    - Administrateurs
    - Gestionnaires/modérateurs
    - Rédacteurs
      - *Comité éditorial défini* (?)
      - *Tous les membres*(?) (Documentation?)
    - Visiteurs
  - Réglementation et données personnelles
    - [ ] Déclaration normale CNIL (article 23 de la loi du 6/01/78 modifiée): https://declarations.cnil.fr/declarations/declaration/declarant.display.action?showDraftPopup=true (ref brouillon: **h3c2444960#**)
    - [ ] Avertissement sur la présence de cookies: 
    - [ ] Traitement fIchier membres assos: https://www.cnil.fr/fr/les-fichiers-des-associations-en-questions
    - [ ] Voir la conformité d'un annuaire des membres
    - [ ] Possibilité de refuser de fournir/rendre publiques certaines infos autres que l'état civil    
(**IMPÉRATIF**, aujourd'hui la collecte **obligatoire** des photos via HelloAsso pour le trombi est **illégale**!!)
  - [ ] Création des articles (contenus statiques)  
Pad de "relecture/contribution" des textes bruts des différentes pages: https://annuel2.framapad.org/p/sitelafabbriquetextes
  - [ ] Dériver un template et appliquer la charte graphique héritée du site V2
       * https://github.com/gsuez/master-bootstrap-3
       * https://github.com/JoomShaper/Helix3
       * ...
  - [ ] Développer le template

## Mise en oeuvre

  * Créer la BDD (ou import de l'Alpha) sur le manager OVH
    - nom: "lfbv3\_joomla"
  * Uploader les fichiers racine de joomla sur le webroot
  * Ouvrir l'URL du webroot et se laisser guider
    - BDD: "lfbv3\_joomla"
    - préfixes: "lfbv3J"
  * -> {$URL}/administrator
  * Mise à jour
  * ...
